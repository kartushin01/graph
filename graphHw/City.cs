﻿using System;
using System.Collections.Generic;
using System.Text;

namespace graphHw
{
    public class City
    {
        public string Name { get; set; }
        public override string ToString() => $"{Name}";
    }
}
