﻿using System;
using System.Collections.Generic;
using System.Text;

namespace graphHw
{
    public class Edge<T, K> : IEdge<T, K> where T : INode<K>
    {
        public T FromNode { get; set; }
        public T ToNode { get; set; }
        public int Weight { get; set; }
        public override string ToString() => $"Range from {FromNode.Data} to {ToNode.Data} - {Weight} km";


    }
}
