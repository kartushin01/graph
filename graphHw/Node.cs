﻿using System;
using System.Collections.Generic;
using System.Text;

namespace graphHw
{
    public class Node<T> : INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; } = default;
        public override bool Equals(object obj) => obj is Node<T> node ? node.Id == this.Id : false;

        public override string ToString() => $"Node {{ Id: {{ {Id} }}, Data: {{ {Data.ToString()} }}";
    }
}
