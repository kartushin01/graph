﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace graphHw
{
    public class RoutResult
    {
        public string StartNode { get; set; }

        public Dictionary<string, int> Points { get; set; }

        public int TotalWay { get; set; }

        public RoutResult()
        {
            Points = new Dictionary<string, int>();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            TotalWay = 0;

            sb.Append($"Направляйтесь из {StartNode} до: \n");

            foreach (var point in Points.Reverse())
            {
                sb.Append($"{point.Key} {point.Value} км \n");
                TotalWay += point.Value;
            }

            sb.Append($"Общий путь {TotalWay} км");
            return sb.ToString();
        }
    }
}
