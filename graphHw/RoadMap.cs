﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace graphHw
{
    public class RoadMap<T, K> : Graph<T, K> where T : INode<K>
    {
        private List<NodeInfo<T>> InfoNodes { get; set; } =
            new List<NodeInfo<T>>();

        /// <summary>
        /// Матрица смежности
        /// </summary>
        public void getMatrix()
        {
            var martix = new int[NodesList.Count, EdgesList.Count];

            foreach (var edge in EdgesList)
            {
                var row = edge.FromNode.Id;
                var column = edge.ToNode.Id;

                martix[row, column] = edge.Weight;
            }

            Matrix = martix;
        }


        /// <summary>
        /// Смежные вершины (соседи)
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public List<NodeInfo<T>> GetAdjacentNodes(INode<K> node)
        {
            List<NodeInfo<T>> result = new List<NodeInfo<T>>();
            foreach (var edge in EdgesList)
            {
                if (edge.FromNode.Id == node.Id)
                {
                    result.Add(InfoNodes.FirstOrDefault(n =>
                        n.Node.Id == edge.ToNode.Id));
                }
            }

            return result;
        }

        public void Init()
        {
            foreach (var node in NodesList)
            {
                InfoNodes.Add(new NodeInfo<T>()
                {
                    Node = node,
                    Visited = false,
                    prevNode = default(T),
                    Sum = int.MaxValue
                });
            }
        }

        public NodeInfo<T> GetNodeInfo(T node)
        {
            return InfoNodes.FirstOrDefault(n => n.Node.Id == node.Id);
        }

        public NodeInfo<T> GetNodeMinSumUnVisited()
        {
            int minSum = int.MaxValue;
            NodeInfo<T> minNode = new NodeInfo<T>();

            foreach (var node in InfoNodes)
            {
                if (node.Visited) continue;
                if (node.Sum < minSum)
                {
                    minSum = node.Sum;
                    minNode = node;
                }
            }

            return minNode;
        }

        public void ShortWay(T start, T finish)
        {
            Init();
            //Установить Sum 0 для стартовой вершины 
            InfoNodes.FirstOrDefault(n => n.Node.Id == start.Id).Sum = 0;

            //Получить минимальную не посещённую
            var currentNode = GetNodeMinSumUnVisited();

            while (currentNode.Node != null)
            {
                SetSumToNextNodes(
                    currentNode); //Установить сумму для всех вершин
                currentNode = GetNodeMinSumUnVisited();
            }

            //Построить маршрут для заданных вершин
            Console.WriteLine(ShowRout(GetNodeInfo(start),
                GetNodeInfo(finish)));
            Console.ReadLine();
        }

        private string ShowRout(NodeInfo<T> start, NodeInfo<T> finish)
        {
            RoutResult res = new RoutResult();
            res.StartNode = start.Node.Data.ToString();

            string path = finish.Node.Data.ToString();

            var edge = GetEdge(finish.prevNode.Id, finish.Node.Id);

            res.Points.Add(finish.Node.Data.ToString(), edge.Weight);
            while (finish.Node.Id != start.Node.Id)
            {
                finish = GetNodeInfo(finish.prevNode);

                if (finish.prevNode != null)
                {
                    edge = GetEdge(finish.prevNode.Id, finish.Node.Id);
                    res.Points.Add(finish.Node.Data.ToString(), edge.Weight);
                }
            }

            return res.ToString();
        }

        private void SetSumToNextNodes(NodeInfo<T> currentNode)
        {
            currentNode.Visited = true;
            //Перебираем смежные вершины
            var neighbors = GetAdjacentNodes(currentNode.Node);
            foreach (var neighbor in neighbors)
            {
                if (!neighbor.Visited)
                {
                    var edge = EdgesList.FirstOrDefault(n =>
                        n.ToNode.Id == neighbor.Node.Id &&
                        n.FromNode.Id == currentNode.Node.Id);
                    var newSum = currentNode.Sum + edge.Weight;
                    if (newSum < neighbor.Sum)
                    {
                        neighbor.Sum = newSum;
                        neighbor.prevNode = currentNode.Node;
                    }
                }
            }

            var t = 2;
        }
    }
}
