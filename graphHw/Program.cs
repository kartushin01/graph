﻿using System;

namespace graphHw
{
    class Program
    {
        static void Main(string[] args)
        {
            var rm = new RoadMap<Node<City>, City>();

            Node<City> moscow = new Node<City>()
            {
                Id = 1,
                Data = new City()
                {
                    Name = "Москва"
                }
            };
            rm.AddNode(moscow);

            Node<City> mytishi = new Node<City>()
            {
                Id = 2,
                Data = new City()
                {
                    Name = "Мытищи"
                }
            };

            rm.AddNode(mytishi);
            Node<City> korolev = new Node<City>()
            {
                Id = 3,
                Data = new City()
                {
                    Name = "Королев"
                }
            };
            rm.AddNode(korolev);
            Node<City> pushkino = new Node<City>()
            {
                Id = 4,
                Data = new City()
                {
                    Name = "Пушкино"
                }
            };
            rm.AddNode(pushkino);

            Node<City> ivanteevka = new Node<City>()
            {
                Id = 5,
                Data = new City()
                {
                    Name = "Ивантеевка"
                }
            };
            rm.AddNode(ivanteevka);

            Node<City> shelkovo = new Node<City>()
            {
                Id = 6,
                Data = new City()
                {
                    Name = "Щелково"
                }
            };
            rm.AddNode(shelkovo);

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = moscow, ToNode = mytishi, Weight = 5
            });
            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = moscow, ToNode = korolev, Weight = 7
            });
            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = moscow, ToNode = pushkino, Weight = 14
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = mytishi, ToNode = pushkino, Weight = 10
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = mytishi, ToNode = ivanteevka, Weight = 12
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = korolev, ToNode = pushkino, Weight = 9
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = ivanteevka, ToNode = shelkovo, Weight = 4
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = korolev, ToNode = shelkovo, Weight = 1
            });

            rm.AddEdge(new Edge<Node<City>, City>()
            {
                FromNode = pushkino, ToNode = shelkovo, Weight = 5
            });


            //rm.getMatrix();

            rm.ShortWay(moscow, ivanteevka);
        }
    }
}
