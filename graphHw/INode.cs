﻿using System;
using System.Collections.Generic;
using System.Text;

namespace graphHw
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; } 
        
    }
}
