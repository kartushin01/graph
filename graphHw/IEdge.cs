﻿using System;
using System.Collections.Generic;
using System.Text;

namespace graphHw
{
    public interface IEdge<T, K> where T : INode<K>
    {
         T FromNode { get; set; }
         T ToNode { get; set; }
         int Weight { get; set; }
    }

}
