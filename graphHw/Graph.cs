﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace graphHw
{
    public class Graph<T, K> where T : INode<K>
    {
        public List<T> NodesList { get; set; }
        public List<Edge<T, K>> EdgesList { get; set; }
        public int[,] Matrix { get; set; }

        public Graph()
        {
            NodesList = new List<T>();
            EdgesList = new List<Edge<T, K>>();
        }

        public void AddNode(T node)
        {
            NodesList.Add(node);
        }

        public void AddEdge(Edge<T, K> edge)
        {
            EdgesList.Add(edge);
        }


        public bool Contains(T node)
        {
            return (NodesList.Any(n => n.Equals(node)));
        }

        public void Remove(T node)
        {
            var nodeToDel = NodesList.FirstOrDefault(n => n.Equals(node));
            if (nodeToDel != null)
                NodesList.Remove(nodeToDel);
        }

        public K GetNodeData(int id)
        {
            var result = NodesList.FirstOrDefault(node => node.Id == id);

            return result == null ? default : result.Data;
        }

        public T GetNode(K data) =>
            NodesList.FirstOrDefault(node => node.Data.Equals(data));


        public Edge<T, K> GetEdge(int idFrom, int idTo)
        {
            return EdgesList.FirstOrDefault(n =>
                n.ToNode.Id == idTo &&
                n.FromNode.Id == idFrom);
        }
    }
}
